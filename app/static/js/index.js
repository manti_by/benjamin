(($) => {

    'use strict';

    $.weather.init();
    $.datetime.init();

    $.plants_latest.init();
    $.plants_report.init();

})(jQuery);